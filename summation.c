///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01d - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Brooke Maeda <bmhm@hawaii.edu>
// @date   14_1_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   int n = atoi(argv[1]); //accept integer
   //initialize
   int sum = 0;
   //loop if i between 1 and n 
   for ( int i = 1; i <= n; i++){
      //then add i to sum
      sum = sum + i;
   }
   //print sum
   printf("The sum of the digits from 1 to %d is %d\n", n, sum);
   
   return 0;
}
